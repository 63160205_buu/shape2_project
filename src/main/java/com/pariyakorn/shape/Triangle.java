/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.shape;

/**
 *
 * @author acer
 */
public class Triangle extends Shape {
    private double b, h;

    public Triangle(double b, double h) {
        System.out.println("Triangle created");
        this.b = b;
        this.h = h;
    }
    public double calArea() {
        return 0.5 * b * h;
    }
    public void print(){
        System.out.println("Triangle: base = " + this.b + " height = " + this.h + " Area = " + calArea());
    }

}
